package com.covid.covid19.controllers;

import com.covid.covid19.models.LocationStats;
import com.covid.covid19.services.CoronaVirusDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {
    @Autowired
    private CoronaVirusDataService coronaVirusDataService;
    @GetMapping("/")
    public String home(Model model){
        List<LocationStats> allStats = coronaVirusDataService.getLocationStats();
        int totalCases = allStats.stream().mapToInt(locationStats -> locationStats.getLatestTotalCases()).sum();
        model.addAttribute("locationStat", allStats);
        model.addAttribute("totalCases",totalCases);

        return "home";
    }
}
