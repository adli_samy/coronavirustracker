package com.covid.covid19.models;

public class LocationStats  {
    private String state;
    private String country;
    private int latestTotalCases;

    private int diffFromPrevDay;

    public LocationStats() {
    }

    public LocationStats(String state, String country, int latestTotalCases) {
        this.state = state;
        this.country = country;
        this.latestTotalCases = latestTotalCases;
    }


    public int getDiffFromPrevDay() {
        return diffFromPrevDay;
    }

    public void setDiffFromPrevDay(int diffFromPrevDay) {
        this.diffFromPrevDay = diffFromPrevDay;
    }


    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setLatestTotalCases(int latestTotalCases) {
        this.latestTotalCases = latestTotalCases;
    }

    public int getLatestTotalCases() {
        return latestTotalCases;
    }
}
